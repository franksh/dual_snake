# Package

version       = "0.1.0"
author        = "Frank S. Hestvik"
description   = "Simple pixelart dual-snake game"
license       = "MIT"
srcDir        = "src"
bin           = @["dual_snake"]

# Dependencies

requires "nim >= 1.4.8"
requires "nimraylib_now >= 0.13"
