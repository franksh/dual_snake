
import pkg/nimraylib_now

import std/hashes

type
  Direction* = enum
    North,
    East,
    South,
    West

  DirSet* = set[Direction]

const
  NorthOnly* = {North}
  SouthOnly* = {South}
  EastOnly* = {East}
  WestOnly* = {West}
  NorthEast* = {North,East}
  NorthWest* = {North,West}
  SouthEast* = {South,East}
  SouthWest* = {South,West}
  NorthSouth* = {North,South}
  EastWest* = {East,West}


func `-`*(d: Direction): Direction =
  case d
  of North: South
  of South: North
  of East: West
  of West: East

func rotR*(d: Direction): Direction =
  case d
  of North: East
  of East: North
  of South: West
  of West: North

func rotL*(d: Direction): Direction =
  case d
  of North: West
  of East: South
  of South: West
  of West: North

func `|?`*(a, b: Direction): bool = a == -b
func `+?`*(a, b: Direction): bool = a == b.rotR or a == b.rotL

type
  Pos* = Vector2

converter toPos*[T,U](xy: tuple[x:T,y:U]): Pos =
  Pos(x: xy.x.cfloat, y: xy.y.cfloat)


func `+`(r: Rectangle, v: Vector2): Rectangle =
  Rectangle(x: r.x + v.x, y: r.y + v.y, width: r.width, height: r.height)
func `/`(v: Vector2, c: float): Vector2 =
  Vector2(x: v.x/c, y: v.y/c)

func hash*(pos: Pos): Hash = hash(pos.x) *% 17 + hash(pos.y)

func step*(p: Pos, d: Direction): Pos =
  case d
  of North: (p.x, p.y-1)
  of East: (p.x+1, p.y)
  of South: (p.x, p.y+1)
  of West: (p.x-1, p.y)

func rotCW(pos: Pos): Pos = (-pos.y, pos.x)
func rotCCW(pos: Pos): Pos = (pos.y, -pos.x)
func rot180(pos: Pos): Pos = (-pos.x, -pos.y)

