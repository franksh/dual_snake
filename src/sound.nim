
import pkg/nimraylib_now


type
  SoundKind* = enum
    Menu, EatShort, EatLong, MoveA, MoveB, Lose


var sounds: array[SoundKind,Sound]

proc initSounds*() =
  sounds[Menu] = loadSound("res/menu_select.wav")
  sounds[EatShort] = loadSound("res/eat_short.wav")
  sounds[EatLong] = loadSound("res/eat_long.wav")
  sounds[MoveA] = loadSound("res/move_0.wav")
  sounds[MoveB] = loadSound("res/move_1.wav")
  sounds[Lose] = loadSound("res/lose.wav")

  setSoundVolume(sounds[MoveA], 0.2)
  setSoundVolume(sounds[MoveB], 0.2)

proc playSound*(sk: SoundKind) =
  playSound(sounds[sk])

proc deinitSounds*() =
  for i in low(sounds)..high(sounds):
    unloadSound(sounds[i])
