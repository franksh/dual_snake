import std/strformat
import std/math
import std/random
# import std/os

import pkg/nimraylib_now
# import pkg/langexts
import tiles
import sound

template `::=`*(v, expr: untyped): untyped =
  ## Like a let statement but can be used as an expression.
  ##
  let v {.inject.} = expr
  v

const
  game_name = "dual_snake"
  screen_width = 1280
  screen_height = 768
  target_fps = 60

  arena_width = 23 # 27
  arena_height = 13 # 15

  tile_width = 16
  tile_height = 16

  camera_zoom = screen_width / (tile_width * arena_width) # 2.962962962962963
  # gutter_height = 56 div 2
  gutter_height = (screen_height - (arena_height * tile_height * camera_zoom)).int
  base_speed = 1.5

  snake_color = [Red, Blue]

  shader_source = """
#version 330

in vec3 vertexPos;
in vec2 fragTexCoord;
in vec4 fragColor;

// Input uniform values
uniform sampler2D texture0;
uniform vec4 colDiffuse;
uniform float time = 0.0;

out vec4 finalColor;

vec2 uv_adjust(vec2 uv, vec2 tex_size, vec2 tile_size, float width) {
  vec2 pixel = uv * tex_size;
  vec2 tile_seam = floor(pixel / tile_size) * tile_size;

  uv = tile_seam + clamp(pixel - tile_seam, vec2(0.5), tile_size - 0.5);

  vec2 uv_floor = floor(uv + 0.5);
  vec2 uv_fract = fract(uv + 0.5);
  vec2 uv_aa = fwidth(pixel) * width * 0.5;
  uv_fract = smoothstep(vec2(0.5) - uv_aa, vec2(0.5) + uv_aa, uv_fract);

  return (uv_floor + uv_fract - 0.5) / tex_size;
}

void main()
{
  vec2 uv = uv_adjust(fragTexCoord, vec2(64,96), vec2(16), 1.5);
  finalColor = colDiffuse * fragColor * texture(texture0, uv);
}
"""

type
  TileKind = enum
    EmptyTile, WallTile, FoodTile, SnakeHead, SnakeBody, SnakeTail

  Tile = object
    kind: TileKind
    dirTo: Direction
    dirFrom: Direction
    which: int
    anim: float

  GameMode = enum
    Paused, Running, GameOver

  Snake = object
    head: Pos
    tail: Pos
    id: int
    speed: float64
    next_update: float64
    length: int
    left_to_grow: int
    score: int

  GameState = object
    tex: Texture
    shader: Shader
    uni_loc: cint

    height, width: int
    board: seq[Tile]
    snakes: array[2, Snake]
    mode: GameMode
    time: float64

var state: GameState
var camera = Camera2D(
  target: (0,0),
  offset: (0,0),
  zoom: camera_zoom,
  rotation: 0.0,
)

# import files
# let sound_files = find_files("/Users/Frank/dev/ef2/resources", ".wav")

# proc setSound(ix: int) =
#   state.sound_idx = (sound_files.len + state.sound_idx + ix) mod sound_files.len
#   state.sound = loadSound(sound_files[state.sound_idx])

const tilemap = [
  [(SnakeHead, SouthOnly), (SnakeHead, EastOnly), (SnakeHead, NorthOnly), (SnakeHead, WestOnly)],
  [(SnakeBody, SouthEast), (SnakeBody, NorthEast), (SnakeBody, NorthWest), (SnakeBody, SouthWest)],
  [(SnakeBody, NorthSouth), (SnakeBody, EastWest), (FoodTile, NorthSouth), (FoodTile, EastWest)],
  [(WallTile, WestOnly), (WallTile, SouthOnly), (WallTile, EastOnly), (WallTile, NorthOnly)],
  [(WallTile, SouthWest), (WallTile, SouthEast), (WallTile, NorthEast), (WallTile, NorthWest)],
  [(SnakeTail, SouthOnly), (SnakeTail, EastOnly), (SnakeTail, NorthOnly), (SnakeTail, WestOnly)],
]

func tileRect(p: Pos): Rectangle =
  Rectangle(
    x: p.x * tile_width,
    y: p.y * tile_height,
    width: tile_width,
    height: tile_height,
  )
func asTile(p: Pos): Pos =
  Pos(x: p.x*tile_width, y: p.y*tile_height)
proc drawTile(p: Pos, k: TileKind, d: DirSet, color: Color): bool =
  for r in low(tilemap)..high(tilemap):
    for c in low(tilemap[r])..high(tilemap[r]):
      if tilemap[r][c][0] == k and tilemap[r][c][1] == d:
        drawTextureRec(state.tex, (c,r).tileRect, p.asTile, color)
        return true
  return false


proc `[]`(gs: var GameState, p: Pos): var Tile =
  let x = p.x.int
  let y = p.y.int
  assert x.float == p.x
  assert y.float == p.y
  assert x >= 0 and x < state.width
  assert y >= 0 and y < state.height
  state.board[y*state.width + x]

proc `[]=`(gs: var GameState, p: Pos, t: Tile) =
  let x = p.x.int
  let y = p.y.int
  assert x.float == p.x
  assert y.float == p.y
  assert x >= 0 and x < state.width
  assert y >= 0 and y < state.height
  state.board[y*state.width + x] = t

func score(gs: GameState): int =
  gs.snakes[0].score + gs.snakes[1].score

proc placeFood(which: int) =
  var q: Pos = (rand(state.width-1), rand(state.height-1))
  while state[q].kind != EmptyTile:
    q = (rand(state.width-1), rand(state.height-1))

  state[q].kind = FoodTile
  state[q].which = which



proc moveSnake(which: int, dir: Direction, play_sound: bool = true) =
  let snake = addr state.snakes[which]
  let h = snake.head
  let hn = h.step dir
  if state[h].dirTo |? dir:
    return
  state[h].dirTo = dir
  state[h].kind = SnakeBody
  if state[hn].kind == FoodTile:
    placeFood(state[hn].which)
    if state[hn].which == which:
      snake.left_to_grow += 5
      snake.score += 100
      playSound(SoundKind.EatLong)
    else:
      snake.left_to_grow += 1
      snake.score += 10
      playSound(SoundKind.EatShort)

  if snake.left_to_grow > 0:
    snake.length += 1
    snake.left_to_grow -= 1
  else:
    let t = snake.tail
    let tn = t.step state[t].dirTo
    state[t].kind = EmptyTile
    state[tn].kind = SnakeTail
    snake.tail = tn

  if state[hn].kind in {WallTile, SnakeHead, SnakeBody, SnakeTail}:
    state.mode = GameOver
    playSound(SoundKind.Lose)
  elif state[hn].kind == EmptyTile and play_sound:
    playSound(if which == 0: SoundKind.MoveA else: SoundKind.MoveB)
  state[hn] = Tile(
    kind: SnakeHead,
    dirTo: dir,
    dirFrom: -dir,
    which: snake.id,
    anim: 2.0,
  )
  snake.head = hn

  if snake.next_update < state.time + 1.0 / snake.speed:
    snake.next_update += 1.0 / snake.speed


proc setSnake(which: int, h: Pos) =
  let t = h.step South
  state.snakes[which] = Snake(
    head: h,
    tail: t,
    id: which,
    speed: 2.0,
    next_update: getTime() + 1.0,
    length: 2,
    left_to_grow: 0,
  )
  state[h] = Tile(
    kind: SnakeHead,
    dirTo: North,
    dirFrom: South,
    which: which,
    anim: 2.0,
  )
  state[t] = Tile(
    kind: SnakeTail,
    dirTo: North,
    which: which,
    anim: 2.0,
  )


proc initState(gs: var GameState) =
  let (w,h) = (gs.width, gs.height)
  state.mode = Running
  state.board = newSeq[Tile](w*h)
  setSnake(0, ((w/3).floor, (3*h/4).floor))
  setSnake(1, ((2*w/3).floor, (3*h/4).floor))
  for x in 0..<w:
    state[(x,0)].kind = WallTile
    state[(x,0)].dirTo = North
    state[(x,0)].dirFrom = North
    state[(x,h-1)].kind = WallTile
    state[(x,h-1)].dirTo = South
    state[(x,h-1)].dirFrom = South
  for y in 0..<h:
    state[(0,y)].kind = WallTile
    state[(0,y)].dirTo = West
    if y != 0 and y != h-1:
      state[(0,y)].dirFrom = West
    state[(w-1,y)].kind = WallTile
    state[(w-1,y)].dirTo = East
    if y != 0 and y != h-1:
      state[(w-1,y)].dirFrom = East
  placeFood(0)
  placeFood(1)

proc initSnake(w, h: int) =
  state.tex = loadTexture("res/tiles.png")
  state.tex.setTextureFilter(BILINEAR)
  state.shader = loadShaderFromMemory(nil, shader_source)
  state.uni_loc = state.shader.getShaderLocation("time")

  state.width = w
  state.height = h
  initState(state)

proc drawEmpty(p: Pos) =
  discard
proc drawSnakeHead(p: Pos, t: Tile) =
  assert drawTile(p, SnakeHead, {t.dirFrom}, snake_color[t.which])
proc drawSnakeTail(p: Pos, t: Tile) =
  assert drawTile(p, SnakeTail, {t.dirTo}, snake_color[t.which])
proc drawSnakeBody(p: Pos, t: Tile) =
  assert drawTile(p, SnakeBody, {t.dirTo,t.dirFrom}, snake_color[t.which])

proc drawFood(p: Pos, t: Tile) =
  assert drawTile(p, FoodTile, (if t.which == 0: NorthSouth else: EastWest), snake_color[t.which])
proc drawWall(p: Pos, t: Tile) =
  assert drawTile(p, WallTile, {t.dirTo,t.dirFrom}, Brown)

proc drawTile(p: Pos) =
  let t = state[p]
  case t.kind
  of EmptyTile: drawEmpty(p)
  of SnakeHead: drawSnakeHead(p, t)
  of SnakeTail: drawSnakeTail(p, t)
  of SnakeBody: drawSnakeBody(p, t)
  of FoodTile: drawFood(p, t)
  of WallTile: drawWall(p, t)


proc drawTextCentered(text: string, pos: Pos, size: int) =
  let xw = measureText(text, size)
  let ur: Pos = (pos.x - xw/2, pos.y - size/2)
  # drawRectangleLines(ur.x.int, ur.y.int, xw, size, Red)
  drawTextEx(getFontDefault(), text, ur, size.float, floor(size / 10), White)
  # drawPixelV(pos, Yellow)

proc drawGameGameOver() =
  drawTextCentered("GAME_OVER", (screen_width / 2, screen_height / 2 - 72), 72)
  drawTextCentered(&"final_score: {state.score}", (screen_width / 2, screen_height / 2), 36)
  drawTextCentered(&"press <SPACE> to restart", (screen_width / 2, screen_height / 2 + 36), 24)

proc drawGamePaused() =
  drawTextCentered("paused", (screen_width / 2, screen_height / 2 - 100.0), 72)
  drawTextCentered("press <SPACE> to continue", (screen_width / 2, screen_height / 2), 36)

proc drawGameRunning() =
  drawRectangleRec(Rectangle(
    x: 0, y: 0,
    width: arena_width * tile_width,
    height: arena_height * tile_height
  ), Color(r: 10, g: 20, b: 10, a: 255))
  beginShaderMode(state.shader):
    for x in 0..<state.width:
      for y in 0..<state.height:
        drawTile((x,y))
  # beginShaderMode(state.shader):
  #   for x in 0..<state.width:
  #     for y in 0..<state.height:
  #       if state[(x,y)].kind in {SnakeHead, SnakeTail, SnakeBody}:
  #         # drawRectangleRec()
  #         discard


proc drawSnake() =
  case state.mode
  of Running:
    beginMode2D(camera):
      drawGameRunning()
  of GameOver:
    drawGameGameOver()
  of Paused:
    drawGamePaused()

proc init*() =
  # setConfigFlags(MSAA_4X_HINT)
  initWindow(screen_width, screen_height, game_name)
  setTargetFPS(target_fps)
  initAudioDevice()

  initSnake(arena_width, arena_height)
  initSounds()

proc deinit*() =
  deinitSounds()
  closeAudioDevice()
  closeWindow()


proc handleInput*(key: KeyboardKey) =
  if state.mode == GameOver:
    if key == SPACE:
      state.initState()
    return
  if state.mode == Paused:
    if key == SPACE:
      state.mode = Running
      for snake in state.snakes.mitems():
        snake.next_update = state.time + 1.0 / snake.speed
    return
  case key
  of SPACE:
    state.mode = Paused
  of W: moveSnake(0, North)
  of A: moveSnake(0, West)
  of S: moveSnake(0, South)
  of D: moveSnake(0, East)
  of I: moveSnake(1, North)
  of J: moveSnake(1, West)
  of K: moveSnake(1, South)
  of L: moveSnake(1, East)
  else:
    discard

func calcSpeed(t, l: int): float64 =
  let (t,l) = (t.float64, l.float64)
  sqrt(50 + 30*t - 20*l)

var t32: cfloat;

proc update*() =
  if state.mode != Running:
    return
  let t = state.time
  t32 = t.cfloat
  state.shader.setShaderValue(state.uni_loc, addr t32, FLOAT)
  let tot = state.snakes[0].length + state.snakes[1].length
  for i in 0..1:
    let snake = addr state.snakes[i]
    while state.mode == Running and snake.next_update < t:
      moveSnake(snake.id, state[snake.head].dirTo, false)
    snake.speed = calcSpeed(tot, snake.length) / calcSpeed(4, 2) * base_speed

proc draw*() =
  clearBackground(Brown)
  drawSnake()

  drawText(&"score: {state.score}", 10, screen_height - gutter_height, gutter_height, White)
  drawText(&"<WASD> and <IJKL> to move, <SPACE> to pause", screen_width div 2, screen_height - gutter_height div 2, gutter_height div 2, White)

proc main*() =
  init()
  while not windowShouldClose():
    state.time = getTime()
    while (q ::= getKeyPressed()) != 0:
      handleInput(cast[KeyboardKey](q))
    update()
    beginDrawing:
      draw()
  deinit()
